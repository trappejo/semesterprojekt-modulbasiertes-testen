import java.util.List;
import java.io.IOException;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;


/**
 * Class that allows for manipulating and outputting a .gazebo.xacro file based on
 * featuremodel info.
 */
public class GazeboAdapter extends Adapter {
	//Variables used exclusively by .gazebo.xacro adapter
	public final String in_gxName;
	public final Document gazeboModel;
	
	/**
	 * Creates a runnable Adapter for .gazebo.xacro files.
	 * 
	 * When reading input in_gxPath may contain other files that will be filtered out.
	 * 
	 * @throws IOException 
	 * @throws JDOMException 
	 */
	public GazeboAdapter() throws JDOMException, IOException {
		super();
		
		//Get the selected .gazebo.xacro file name as String
		in_gxName = botName+GX_ENDING;
		
		//Get the selected .gazebo.xacro file as Document
		List<String> fileMatchList = XMLHelper.findFile(in_gxName, in_gxPath);
		
		if(fileMatchList.isEmpty()) {
			System.err.println("Cannot handle \'"+in_gxName+"\': No input file known.");
			System.exit(-1);
		}
		if(fileMatchList.size()!=1) {
			System.err.println("Cannot handle \'"+in_gxName+"\': Multiple input files found:");
			for(String match : fileMatchList) {
				System.err.println("\'"+match+"\'");
			}
			System.exit(-1);
		}
		
		gazeboModel = XMLHelper.readXml(fileMatchList.get(0));
	}
	
	/**
	 * Run the Adapter for .gazebo.xacro files.
	 * Depending on the selected mode, this Adapter will either:
	 * 	(NO_CHANGE)	- Do nothing.
	 * 
	 * 	(Custom)	- Remove all existing sensors in gazeboModel template.
	 * 				- Insert only the selected sensors.
	 * 
	 * @throws IOException 
	 */
	public void run() throws IOException {
		System.out.println("GazeboAdapter started.");
		
		switch(this.mode) {
		case "NO_CHANGE":
			break;
		case "Custom":
			//Remove all existing sensors in template
			this.removeExistingSensors();
			
			//Insert selected sensors into template
			this.insertSelectedSensors();
			break;
		default:
			System.err.println("Invalid input in \'"+this.in_fmName+"\': NO_CHANGE/ Custom parameter could not be parsed.");
			System.exit(-1);
		}
		
		//Output gazeboModel
		this.output();
		
		System.out.println("GazeboAdapter finished.");
	}
	
	/**
	 * Removes all existing sensors (with its enclosing {@code <gazebo>} tag) from gazeboModel.
	 */
	private void removeExistingSensors() {
		List<Element> removableSensorsList = XMLHelper.xQueryElements(this.gazeboModel, "//sensor");
		
		for(Element sensor : removableSensorsList) {	
			Element entry = sensor.getParentElement();
			if(entry.getName().equals("gazebo")) {
				entry.detach();
			}
			else {
				System.out.println("Warning: Found a <sensor> element in \'"+this.in_gxName+
					"\' that does not have a <gazebo> element as immediate parent. It was not removed, as this input is unexpected.");	
			}
		}
	}
	
	/**
	 * Inserts all selected sensors (with its enclosing {@code <gazebo>} tag) into gazeboModel.
	 */
	private void insertSelectedSensors() {
		for(Document sensorConfig : this.sensorConfigList) {	
			List<Element> sensorList = XMLHelper.xQueryElements(sensorConfig, "//gazebo");
			
			if(sensorList.size()!=1) {
				System.err.println("Invalid input. At least one sensorconfig file contains no or multiple <gazebo> entries.");
				System.exit(-1);	
			}
			
			Element sensor = sensorList.get(0);
			
			//Warn if a sensor still contains $(arg ...) values
			checkForArg(sensor);
			
			sensor.detach();
			this.gazeboModel.getRootElement().addContent(sensor);
			
			
		}
	}
	
	/**
	 * Checks if an Element still contains $(arg ...) values. 
	 * Only exists because because someone might manually create a sensorconfig by copying
	 * from a .gazebo.xacro template and forget that $(arg ...) values are still inside.
	 */
	private static boolean checkForArg(Element element) {
		boolean detected = false;
		List<Element> tagList = XMLHelper.xQueryElements(element, "//*");
		
		for(Element tag : tagList) {	
			String text = tag.getText();
			if(text.contains("$")) {
				System.out.println("Warning: A sensorconfig file still contains an abstract value: \'"+text+
					"\'. It should be replaced with a set value, or the output .gazebo.xacro will likely be broken.");
				detected = true;
			}
		}
		
		return detected;
	}
	
	/**
	 * Output the gazeboModel as XML file.
	 * 
	 * @throws IOException 
	 */
	private void output() throws IOException {
		XMLHelper.saveAsXml(this.gazeboModel, this.out_gxPath+this.out_gxName);
	}
}
