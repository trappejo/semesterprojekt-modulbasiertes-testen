
import java.util.List;
import java.util.ArrayList;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.File;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;



/**
 * Main class for the car configurator.
 * Is refined by modules, which add selected components to component list.
 * This class has to save a file containing the configuration
 * and launch the test seletion application afterwards.
 * 
 * 
 * Naming conventions: 
 * m_: Member variable
 * s: String
 * n: Integer number (int, long,...)
 * f: Floating point number (float, double,...)
 * o: Object
 * a: Collection (List, Array,...)
 * 
 */

abstract class Configurator$$ConfiguratorMain {	
	//module list
	private List m_aModuleList;
	//output path
	private String m_sOutputPath = "resources/output/featuremodel.xml";
	//path to the feature model xml
	private String m_sFeatureModelPath = "model.xml";
	//path to the directory of test manager
	private String m_sTestManagerPath = "../../../build/sim/";
	//path to the directory of testcase compliler
	private String m_sTestCaseCompilerPath = "../../../build/test/compiler/";
	//document representation of the feature model
	private Document m_oFeatureModelDoc; 

	//representation of a single module with all of its types
	class Module {
		String sModuleName;
		ArrayList aModuleTypes;
		String sType = null;
	}	
	
	public static void main(String[] args) {
		new Configurator().save();
	}
	
	public void save() {						
		//create empty xml
		Element oSpecs = new Element("specification");
		Document oDoc = new Document(oSpecs);
		//create templates node
		Element oTemplates = new Element("templates");
		oSpecs.addContent(oTemplates);
		//create sensors node
		Element oSensors = new Element("sensors");
		oSpecs.addContent(oSensors);
		//create mode node
		Element oMode = new Element("mode");
		oSensors.addContent(oMode);
		//check if mode should be set to custom
		boolean bCustomSenors = false;
		
		//if the module list was initialized
		if(m_aModuleList!=null) {
			for(int i=0; i<m_aModuleList.size(); i++) {
				Module oModule = (Module)m_aModuleList.get(i);
				//set template
				if(oModule.sType.equals("TEMPLATE")) {
					oTemplates.setAttribute("name", oModule.sModuleName);
				}
				//add sensor node to "sensors" node
				else if(oModule.sType.equals("SENSOR")) {
					Element oElement = new Element("sensor");
					oElement.setAttribute("name", oModule.sModuleName);
					oSensors.addContent(oElement);
					bCustomSenors = true;
				}
				//set selected mode
				else if(oModule.sType.equals("MODE")) {
					oMode.setAttribute("param", oModule.sModuleName);
				}
				//error
				else {
					System.out.print("ERROR: Module type not specified. Module will be ignored");
				}
			}
			//check if the mode should be custom
			if(bCustomSenors||!oMode.hasAttributes())
				oMode.setAttribute("param", "Custom");
		}
		
		System.out.println(oDoc.toString());
		
		//generate xml String and save it to a file
		try {
			XMLHelper.saveAsXml(oDoc, m_sOutputPath);	
		}
		catch (Exception oEx) {
			System.out.println(oEx);
			return;
		}
		
		//create and launch adapters
		try {
			GazeboAdapter oGazeboAdapter = new GazeboAdapter();
			oGazeboAdapter.run();
			UrdfAdapter oUrdfAdapter = new UrdfAdapter();
			oUrdfAdapter.run();
			BotNameAdapter oBotNameAdapter = new BotNameAdapter();
			oBotNameAdapter.run();
		}
		catch (Exception oEx) {
			System.out.println(oEx);
			return;
		}
		
		Runtime oRuntime = Runtime.getRuntime();
		
		//TODO launch compiler
		try {
			File oWorkingDir = new File(m_sTestCaseCompilerPath);
			Process oTestCaseCompiler = oRuntime.exec("./compile", null, oWorkingDir);
			BufferedReader oOutput = new BufferedReader(new InputStreamReader(oTestCaseCompiler.getInputStream()));
			String sOutput = null;
			while ((sOutput = oOutput.readLine()) != null) {
			    System.out.println(sOutput);
			}
		} catch (IOException oEx) {
			System.out.println("ERROR: launching Testcase Compiler failed: " + oEx.toString());
		}
		
		//TODO launch test manager
		try {
			File oWorkingDir = new File(m_sTestManagerPath);
			Process oTestManager = oRuntime.exec("./testmanager.sh", null, oWorkingDir);
			BufferedReader oOutput = new BufferedReader(new InputStreamReader(oTestManager.getInputStream()));
			String sOutput = null;
			while ((sOutput = oOutput.readLine()) != null) {
			    System.out.println(sOutput);
			}
		} catch (IOException oEx) {
			System.out.println("ERROR: launching TestManager failed: " + oEx.toString());
		}
	}
	
	//get all parents of a given module from the Feature Model file
	private Module GetModuleParents(String sModuleName) throws IOException, JDOMException {
		Module oModule = new Module();
		//set name
		oModule.sModuleName = sModuleName;
		//init Doc if not done
		if(m_oFeatureModelDoc==null) {
			m_oFeatureModelDoc = XMLHelper.readXml(m_sFeatureModelPath);
			System.out.println(m_oFeatureModelDoc.toString());
		}
		//get element
		String sQuery = "//*[@name= '"+sModuleName+"']";
		List<Element> aModules = XMLHelper.xQueryElements(m_oFeatureModelDoc, sQuery);
		Element oModuleElement;
		try {
			oModuleElement = aModules.get(0);
		}
		catch (Exception oEx) {
			System.out.println(oEx.toString() + "\nList empty for query "+ sQuery);
			return null;
		}
		//get all anchestors
		Element oParent = oModuleElement.getParentElement();
		ArrayList<String> aTypes = new ArrayList();
		//itterate through parents of module
		while(true) {
			String sElementName = oParent.getAttribute("name").getValue();
			//break if we reach the Main class, as there are no types further up
			if(sElementName.equals("ConfiguratorMain")) {
				break;
			}
			aTypes.add(sElementName);
			//iterate further trough List
			oModuleElement = oParent;
			oParent = oModuleElement.getParentElement();
		}		
		oModule.aModuleTypes = aTypes;
		return oModule;
	}
	
	//add a module name  and module type to the feature list. Init list, if not already done
	public void addToModuleList(String sModuleName) {
		//init List, if not already done
		if(m_aModuleList==null) {
			m_aModuleList = new ArrayList();
		}		
		//get the module types
		Module oModule;
		try {
			oModule = GetModuleParents(sModuleName);		
		}
		catch (Exception oEx) {
			System.out.print(oEx);
			return;
		}		
		//set module type based on abstract features
		if(oModule.aModuleTypes.contains("Templates"))
			oModule.sType = "TEMPLATE";
		else if(oModule.aModuleTypes.contains("Sensors")&&oModule.aModuleTypes.contains("Custom"))
			oModule.sType = "SENSOR";
		else if(oModule.aModuleTypes.contains("Sensors"))
			oModule.sType = "MODE";
		m_aModuleList.add(oModule);
	}
}

/**
 * TODO description
 */
abstract class Configurator$$turtlebot3_waffle extends  Configurator$$ConfiguratorMain  {
	public void save() {
		super.addToModuleList("turtlebot3_waffle");
		super.save();
	}
}

/**
 * TODO description
 */
public class Configurator extends  Configurator$$turtlebot3_waffle  {
	public void save() {
		super.addToModuleList("NO_CHANGE");
		super.save();
	}
}