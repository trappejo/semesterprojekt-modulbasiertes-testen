import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.jdom2.JDOMException;


/**
 * Class that allows for manipulating and outputting the selected bots name.
 */
public class BotNameAdapter extends Adapter {
	//Variables used exclusively by botName adapter
	public final String alteredBotName;
	
	/**
	 * Creates a runnable Adapter for the "botname.txt"(out_bnName) output file.
	 * 
	 * @throws IOException 
	 * @throws JDOMException 
	 */
	public BotNameAdapter() throws JDOMException, IOException {
		super();
		
		//Get the altered botName (botName with unwantedPrefix and unwantedSuffix removed)
		String tmp = botName;
		if(tmp.startsWith(unwantedPrefix)) {
			tmp = tmp.substring(unwantedPrefix.length(), tmp.length());
		}
		else {
			System.out.println("Warning: Prefix  \'"+unwantedPrefix+"\' could not be removed from bot name.");
		}
		if(tmp.endsWith(unwantedSuffix)) {
			tmp = tmp.substring(0, tmp.length() - unwantedSuffix.length());
		}
		else {
			System.out.println("Warning: Suffix  \'"+unwantedSuffix+"\' could not be removed from bot name.");
		}
		
		alteredBotName = tmp;
	}
	
	/**
	 * Run the Adapter for the "botname.txt"(out_bnName) output file.
	 * 
	 * @throws IOException 
	 */
	public void run() throws IOException {
		System.out.println("BotNameAdapter started.");
		
		//Output alteredBotName
		this.output();
		
		System.out.println("BotNameAdapter finished.");
	}
	
	/**
	 * Output alteredBotName as "botname.txt"(out_bnName) file.
	 * 
	 * @throws IOException 
	 */
	private void output() throws IOException {
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(this.out_bnPath+this.out_bnName));
		bufferedWriter.write(this.alteredBotName);
		bufferedWriter.close();
	}

}
