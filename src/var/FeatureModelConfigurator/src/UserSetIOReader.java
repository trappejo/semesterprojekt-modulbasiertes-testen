import java.util.List;
import java.io.IOException;

import org.jdom2.Document;
import org.jdom2.Attribute;
import org.jdom2.JDOMException;


/**
 * Class that allows for reading input from usersetio.config.
 */
public class UserSetIOReader {
	//File endings
	public static final String SC_ENDING = ".xml";
	public static final String GX_ENDING = ".gazebo.xacro";
	public static final String UX_ENDING = ".urdf.xacro";
	
	//Input/ output as defined by user
	public final String in_gxPath;
	
	public final String in_uxPath;
	
	public final String out_gxName;
	public final String out_gxPath;
	
	public final String out_uxName;
	public final String out_uxPath;
	
	public final String out_bnName;
	public final String out_bnPath;
	public final String unwantedPrefix;
	public final String unwantedSuffix;
	
	/**
	 * Constructor reading from default usersetio.config location.
	 * 
	 * @throws IOException 
	 * @throws JDOMException 
	 */
	public UserSetIOReader() throws JDOMException, IOException {
		this("./../usersetio.config");
	}
	
	/**
	 * Constructor reading all values from usersetio.config. 
	 * io.config contains all values that the user will want to set manually.
	 * 
	 * @param path		Location of usersetio.config.
	 * @throws IOException 
	 * @throws JDOMException 
	 */
	private UserSetIOReader(String path) throws JDOMException, IOException {
		Document config = XMLHelper.readXml(path);

		in_gxPath = getConfigVal(config, "input/gazebo_xacro", "path");
		
		in_uxPath = getConfigVal(config, "input/urdf_xacro", "path");
		
		out_gxName = getConfigVal(config, "output/gazebo_xacro", "name");
		out_gxPath = getConfigVal(config, "output/gazebo_xacro", "path");
		
		out_uxName = getConfigVal(config, "output/urdf_xacro", "name");
		out_uxPath = getConfigVal(config, "output/urdf_xacro", "path");
		
		out_bnName = getConfigVal(config, "output/botname", "name");
		out_bnPath = getConfigVal(config, "output/botname", "path");
		unwantedPrefix = getConfigVal(config, "output/botname", "remove_prefix");
		unwantedSuffix = getConfigVal(config, "output/botname", "remove_suffix");
	}
	
	/**
	 * Obtains an attribute value of the input/ output entries specified in *.config.
	 * 
	 * @param doc		The JDOM Document (*.config)
	 * @param entry		String formed as "{input or output}/{entity}"
	 * @param val		Attribute name as String
	 * @return XML-attribute specified in entry of doc (*.config) as String
	 */
	public static String getConfigVal(Document doc, String entry, String val) {
		List<Attribute> valList = XMLHelper.xQueryAttributes(doc, "//"+entry+"/@"+val);
		if(valList.size()!=1) {
			System.err.println("Error while reading input/ output \'"+val+"\' value from *.config.");
			System.exit(-1);
		}

		return valList.get(0).getValue();
	}
}
