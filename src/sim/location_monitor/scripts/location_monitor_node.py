#!/usr/bin/env python

import math
import sys
import gc
import rospy
import re
import array as arr
import numpy as np
import sys, traceback
import multiprocessing
import time
import logging
import os
from datetime import datetime
from geometry_msgs.msg import Quaternion
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from nav_msgs.msg import Odometry
from gazebo_msgs.srv import GetModelState
from gazebo_msgs.srv import GetWorldProperties

BotDist = float(sys.argv[1])#Abstand vom BotKoordinatenursprung zum Kollisions Rand
BotHeight = float(sys.argv[2])#Hoehe des Bots        
BotSides = float(sys.argv[3])#Distanz Zur Ecke.      
BotSideRad = float(sys.argv[4])#Ausrichtungs Radius. 
SafeDist = float(sys.argv[5])#Sicherheitsabstand
DestinationX = float(sys.argv[6])#Ziellinie
Timeout = float(sys.argv[7])#timeout
yawBot = 0.0
WriteFlag = 0

SaveBotX = 0.0
SaveBotY = 0.0
SaveSimtime = 0
SimTime = 0


logger = logging.getLogger("EventLog")
logger.setLevel(logging.INFO)

# create a file handler
handler = logging.FileHandler('location_monitor.log', mode='w')
handler.setLevel(logging.INFO)

# create a logging format
formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# add the file handler to the logger
logger.addHandler(handler)


# y
# ^
# |
# |  ------- <-Box              Box muss im rechtem Winkel zur x-Achse sein!
# |  |     |        2  1  2     Kollisionspunkte des Bots falls BotSides != 0
# |  |     |         \ | /
# |  -------           U   <-Bot
# |
# ---------------->x


class Block:
    def __init__(self, name, relative_entity_name, radius):
        self._name = name
        self._relative_entity_name = relative_entity_name
        self._radius = radius


class Detection:
    _blockListDict = {}
    def show_gazebo_models(self, cordlist):
     global Timeout
     global DestinationX
     global WriteFlag
     global logger
     global SimTime
     global SaveSimtime
     global SaveBotX
     global SaveBotY

     get_world_properties = rospy.ServiceProxy('/gazebo/get_world_properties', GetWorldProperties)
     resp1 = get_world_properties()
     SimTime = resp1.sim_time

     check = 0

     if SaveBotX != float("{0:.2f}".format(cordlist[0])) or SaveBotY != float("{0:.2f}".format(cordlist[1])):
         SaveBotX = float("{0:.2f}".format(cordlist[0]))
         SaveBotY = float("{0:.2f}".format(cordlist[1]))
         SaveSimtime = SimTime
         check = 1

     #print(str(check))
     if check == 0 and ((SimTime-SaveSimtime)>5.0) and SaveSimtime >0.0 and SimTime >0.0:
         logger.info("[T:" + str(resp1.sim_time) + "] " + "Bot did not move for 5 seconds!")
         os._exit(1)


     if resp1.sim_time >= Timeout:
         rospy.loginfo("Simulation Timeout!")
         if WriteFlag == 0:
            WriteFlag = 1
            logger.info("[T:" + str(resp1.sim_time) + "] " + "Simulation Timeout!")
            os._exit(1)
     if WriteFlag == 0:
         logger.info("[T:" + str(resp1.sim_time) + "] " + "Bot X: " + str(cordlist[0]))
         logger.info("[T:" + str(resp1.sim_time) + "] " + "Bot Y: " + str(cordlist[1]))
     if cordlist[0] >= DestinationX and DestinationX != 0.0:
         rospy.loginfo("Bot reached Destination!")
         if WriteFlag == 0:
            WriteFlag = 1
            logger.info("[T:" + str(resp1.sim_time) + "] " + "Bot reached Destination!")
            os._exit(1)
     if cordlist[0] > 0 and cordlist[1] > 0:
        i = 0
        for model in resp1.model_names:
            if ("ball" in str(model)) or ("cylinder" in str(model)) or ("box" in str(model) or ("sphere" in str(model))):
                if("R_" in str(model)):
                    keyword = "R_"
                    search = str(model)
                    afsearch = search.partition(keyword)
                    matches = re.findall(r"[-+]?\d*\.\d+|\d+", afsearch[2])
                else:
                    keyword = "S_"
                    search = str(model)
                    afsearch = search.partition(keyword)
                    matches = re.findall(r"[-+]?\d*\.\d+|\d+", afsearch[2])
                rad = 0
                if matches:
                    if "box" in str(model):
                        rad = (matches[0])
                        rad = float(rad) / 2.0
                    else:
                        rad = (matches[0])
                s = 'block_' + str(i)
                self._blockListDict.update({s: Block(model, 'link', rad)})
                i = i + 1
        try:
            model_coordinates = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
            for block in self._blockListDict.itervalues():
                blockName = str(block._name)
                if blockName in resp1.model_names:
                    resp_coordinates = model_coordinates(blockName, block._relative_entity_name)
                    l_x = resp_coordinates.pose.position.x
                    l_y = resp_coordinates.pose.position.y
                    rospy.loginfo(blockName)
                    rospy.loginfo("X: " + str(l_x))
                    rospy.loginfo("Y: " + str(l_y))
                    if WriteFlag == 0:
                        logger.info(str(blockName))
                        logger.info("[T:" + str(resp1.sim_time) + "] " + str(blockName) + " X: " + str(l_x))
                        logger.info("[T:" + str(resp1.sim_time) + "] " + str(blockName) + " Y: " + str(l_y))
                    if block._radius != 0:
                        if "box" in str(blockName):
                            box_orient_list = [resp_coordinates.pose.orientation.x,resp_coordinates.pose.orientation.y,resp_coordinates.pose.orientation.z,resp_coordinates.pose.orientation.w]
                            (roll, pitch, yaw) = euler_from_quaternion(box_orient_list)
                            yaw_degrees = yaw * 180.0 / math.pi
                            if yaw_degrees < 0:
                                yaw_degrees += 360.0
                            boxkdetection(float(block._radius), cordlist, l_x, l_y, yaw_degrees, resp1.sim_time)
                        else: #if "sphere" , "cylinder" , "ball" etc.
                            objectWithRadiusDetection(float(block._radius), cordlist, l_x, l_y, str(resp1.sim_time), blockName)
                    else:
                        if WriteFlag == 0:
                            logger.info("[T:" + str(resp1.sim_time) + "] " + "Can't detect " + blockName)

        except rospy.ServiceException as e:
            rospy.loginfo("Get Model State service call failed:  {0}".format(e))
     else:
         rospy.loginfo("Bot out of tracking Range!((x,y)<0)")
         if WriteFlag == 0:
             logger.info("[T:" + str(resp1.sim_time) + "] " + "Bot out of tracking Range!((x,y)<0)")


def distance(x1, y1, x2, y2):
    dist = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
    return dist

def objectWithRadiusDetection(radius, cordlist, l_x, l_y, simtime, blockname):
    global WriteFlag
    global logger
    global SafeDist
    global BotHeight
    safedist = radius + SafeDist  # Radius/Size + Bot front Rad + safe dist
    rospy.loginfo("Safedist: " + str(SafeDist))
    if WriteFlag == 0:
        logger.info("Safedist.: " + str(SafeDist))
    lenght = len(cordlist)

    distList = []
    for i in range(0,lenght,2):
       dist = distance(cordlist[i], cordlist[i+1], l_x, l_y)
       distList.append(dist)

    temp = min(distList)
    #print(distList)
    if "ball" in blockname or "sphere" in blockname:
      if radius > BotHeight:
         both = BotHeight
         h = radius-both
         f = temp
         g = pow(h,2) + pow(f,2)
         g = math.sqrt(g)
         if WriteFlag == 0:
             logger.info("[T:" + str(simtime) + "] " + "Dist. To nearest Bot Collision point: " + str(g - radius))
         rospy.loginfo("Dist to collision point: " + str(g - radius))
         if temp < safedist and g < safedist:
             rospy.loginfo("Collision!")
             if WriteFlag == 0:
                 logger.info("[T:" + str(simtime) + "] " + "Collision!")
                 os._exit(1)
      else:
          if WriteFlag == 0:
              logger.info("[T:" + str(simtime) + "] " + "Dist. To nearest Bot Collision point: " + str(temp - radius))
          rospy.loginfo("Dist to collision point: " + str(temp - radius))
          if temp < safedist:
              rospy.loginfo("Collision!")
              if WriteFlag == 0:
                  logger.info("[T:" + str(simtime) + "] " + "Collision!")
                  os._exit(1)
    else:#cylinder
        if WriteFlag == 0:
            logger.info("[T:" + str(simtime) + "] " + "Dist. To nearest Bot Collision point: " + str(temp - radius))
        rospy.loginfo("Dist to collision point: " + str(temp - radius))
        if temp < safedist:
            rospy.loginfo("Collision!")
            if WriteFlag == 0:
                logger.info("[T:" + str(simtime) + "] " + "Collision!")
                os._exit(1)

def boxkdetection(size, cordlist, l_x, l_y, yaw_degrees, simtime):
    global WriteFlag
    global SafeDist
    if (size * 2.0) > 5.0:
        rospy.loginfo("Box to big, > 5m")
        if WriteFlag == 0:
            logger.info("[T:" + str(simtime) + "] " + "Box Invalid (size > 5m)")
    else:
        rospy.loginfo("Safedist: " + str(SafeDist))
        if WriteFlag == 0:
            logger.info("Safedist.: " + str(SafeDist))
        xR = l_x + size * math.cos(math.radians(yaw_degrees))
        yL = l_y + size * math.sin(math.radians(yaw_degrees))

        lenght = len(cordlist)
        distList = []

        for i in range(0,lenght,2):
          lineA = ((cordlist[i], cordlist[i+1]), (l_x, l_y))
          lineB = ((l_x, l_y), (xR, yL))

          angle = angle_between(lineA, lineB)
          disangle = angle
          angle1 = angle
          if angle > 45:
              disangle = 90 - angle;
          dis = (size) / math.cos(math.radians(disangle))
          if cordlist[i] < l_x and cordlist[i+1] > l_y:
              angle1 = 180 - angle
          if cordlist[i] < l_x and cordlist[i+1] < l_y:
              angle1 = 180 + angle
          if cordlist[i] > l_x and cordlist[i+1] < l_y:
              angle1 = 360 - angle

          dynX = l_x + dis * math.cos(math.radians(angle1))
          dynY = l_y + dis * math.sin(math.radians(angle1))

          distult = distance(cordlist[i], cordlist[i+1], dynX, dynY)
          distList.append(distult)

        temp = min(distList)
        #print(distList)
        if WriteFlag == 0:
           logger.info("[T:" + str(simtime) + "] " + "Dist. To nearest Bot Collision point: " + str(temp))
        rospy.loginfo("Dist to collision point: " + str(temp))
        if temp < SafeDist:
           rospy.loginfo("Collision!")
           if WriteFlag == 0:
              logger.info("[T:" + str(simtime) + "] " + "Collision!")
              os._exit(1)


def slope(x1, y1, x2, y2):  # Line slope given two points:
    return (y2 - y1) / (x2 - x1)


def angle(s1, s2):
    return math.degrees(math.atan((s2 - s1) / (1 + (s2 * s1))))


def angle_between(lineA, lineB):
    slope1 = slope(lineA[0][0], lineA[0][1], lineA[1][0], lineA[1][1])
    slope2 = slope(lineB[0][0], lineB[0][1], lineB[1][0], lineB[1][1])

    ang = angle(slope1, slope2)
    if ang < 0:
        ang = ang * (-1.0)

    return ang

def get_rotation (msg):
    global yawBot
    orientation_q = msg.pose.pose.orientation
    orientation_list = [orientation_q.x, orientation_q.y, orientation_q.z, orientation_q.w]
    (roll, pitch, yaw) = euler_from_quaternion (orientation_list)
    yaw_degrees = yaw * 180.0 /math.pi
    if yaw_degrees <0:
        yaw_degrees += 360.0
    #rospy.loginfo("Yaw:" + str(yaw_degrees))
    yawBot = yaw_degrees

def callback(msg):
    global BotDist
    global yawBot
    global BotSides
    global BotSideRad

    get_rotation(msg)
    botx = msg.pose.pose.position.x
    boty = msg.pose.pose.position.y

    botx1 = botx + BotDist * math.cos(math.radians(yawBot))
    boty1 = boty + BotDist * math.sin(math.radians(yawBot))
    rospy.loginfo("BotX: " + str(botx))
    rospy.loginfo("BotY: " + str(boty))
    detect = Detection()

    cordlist = [botx1,boty1]

    if BotSides !=0:
       cordlist.append(botx + BotSides * math.cos(math.radians(yawBot+BotSideRad)))
       cordlist.append(boty + BotSides * math.sin(math.radians(yawBot+BotSideRad)))

       cordlist.append(botx + BotSides * math.cos(math.radians(yawBot-BotSideRad)))
       cordlist.append(boty + BotSides * math.sin(math.radians(yawBot-BotSideRad)))

        # weitere Kollisionspunkte hinzufuegen falls noetig mittels:
        # cordlist.append(botx + (Laenge) * math.cos(math.radians(yawBot(+-)(Radius auf vom Zentrum aus))))
        # cordlist.append(boty + (Laenge) * math.sin(math.radians(yawBot(+-)(Radius auf vom Zentrum aus))))




    detect.show_gazebo_models(cordlist)
    gc.collect()




def main():
    rospy.init_node('location_monitor') #Node in gang bringen
    try:
        rospy.Subscriber("/odom", Odometry, callback) #Koordinaten des bots werden durch /odom geschickt und an callback gesendet
        gc.collect()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass

if __name__ == '__main__':
    main()
