#!/usr/bin/env python

import rospy
import logging
import os
from std_msgs.msg import String
from datetime import datetime
from gazebo_msgs.srv import GetModelState
from gazebo_msgs.srv import GetWorldProperties

logger = logging.getLogger("EventLog")
logger.setLevel(logging.INFO)

# create a file handler
handler = logging.FileHandler('bot_listener.log', mode='w')
handler.setLevel(logging.INFO)

# create a logging format
formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# add the file handler to the logger
logger.addHandler(handler)

def callback(data):
    global logger
    rospy.loginfo(data.data)
    get_world_properties = rospy.ServiceProxy('/gazebo/get_world_properties', GetWorldProperties)
    resp1 = get_world_properties()
    logger.info("[T:" + str(resp1.sim_time) + "] " + data.data)

def listener():
    rospy.init_node('listener', anonymous=True)

    rospy.Subscriber('chatter', String, callback)

    rospy.spin()

if __name__ == '__main__':
    listener()
