﻿using System.Collections.Generic;

namespace compiler
{
    public static class TestWriter
    {
        public static void WriteAll(string path, TestParser.Node rootNode)
        {
            var writers = new List<IWriter>();
            writers.Add(new ConfigWriter());
            writers.Add(new WorldWriter());

            foreach (var writer in writers)
            {
                writer.Write(path, rootNode);
            }
        }
    }
}
