﻿using System;
using System.IO;
using System.Xml.Linq;

namespace compiler
{
    public class WorldWriter : IWriter
    {
        public void Write(string path, TestParser.Node rootNode)
        {
            var world = CreateBaseWorld();

            bool vehicleFound = false;
            foreach (var node in rootNode)
            {
                if (node.Text.StartsWith("spawn:", StringComparison.CurrentCultureIgnoreCase))
                {
                    var text = node.Text.Substring("spawn:".Length).Trim();
                    if (text.StartsWith("Vehicle", StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (vehicleFound)
                        {
                            ConsoleLogger.WriteLine("Multiple Vehicles found", Verbosity.Error);
                            throw new ProcessingException();
                        }
                        vehicleFound = true;//spawn the vehicle always at 0 0 0
                    }
                    else if (text.StartsWith("Obstacle", StringComparison.CurrentCultureIgnoreCase))
                    {
                        try
                        {
                            var obstacle = this.CreateObstacle(new Obstacle(text));
                            world.Add(obstacle);
                        }
                        catch (Exception ex)
                        {
                            ConsoleLogger.WriteLine("Error in creating an obstacle. Are all necessary keys set?", Verbosity.Error);
                            throw new ProcessingException(null, ex);
                        }
                    }
                    else
                    {
                        ConsoleLogger.WriteLine("Unknown spawn-Type: " + text, Verbosity.Error);
                        throw new ProcessingException();
                    }
                }
            }

            if (!vehicleFound)
            {
                ConsoleLogger.WriteLine("No Vehicles found", Verbosity.Error);
                throw new ProcessingException();
            }

            var doc = this.CreateDocumentFromWorld(world);
            var targetFile = Path.Combine(path, "test.world");
            doc.Save(targetFile);
        }

        private XDocument CreateDocumentFromWorld(XElement world)
        {
            var doc = new XDocument(XElement.Parse("<sdf version='1.6'></sdf>"));
            doc.Root.Add(world);
            return doc;
        }

        private XElement CreateBaseWorld()
        {
            return XElement.Parse(@"
  <world name='default'>
    
	<!-- Ground Plane -->
    <include>
      <uri>model://ground_plane</uri>
    </include>

    <include>
      <uri>model://sun</uri>
    </include>
</world>");
        }

        private XElement CreateObstacle(Obstacle obstacle)
        {
            switch (obstacle.Type)
            {
                case "box":
                    return XElement.Parse(@$"<model name='box_{obstacle.Name}_R_{obstacle.Parameter["s"]}'>
                                              <pose>{obstacle.Parameter["x"]} {obstacle.Parameter["y"]} 0 0</pose>
                                              <link name='link'>
                                                <collision name='collision'>
                                                  <geometry>
                                                    <box>
                                                      <size>{obstacle.Parameter["s"]} {obstacle.Parameter["s"]} {obstacle.Parameter["s"]}</size>
                                                    </box>
                                                  </geometry>
                                                </collision>
		
                                                <visual name='visual'>
                                                  <geometry>
                                                    <box>
                                                      <size>{obstacle.Parameter["s"]} {obstacle.Parameter["s"]} {obstacle.Parameter["s"]}</size>
                                                    </box>
                                                  </geometry>
                                                </visual>
                                              </link>
                                            </model>");
                case "sphere":
                    return XElement.Parse(@$"<model name='sphere_{obstacle.Name}_R_{obstacle.Parameter["r"]}'>
                                              <pose>{obstacle.Parameter["x"]} {obstacle.Parameter["y"]} {double.Parse(obstacle.Parameter["r"]) / 2} 0</pose>
                                              <link name='link'>
                                                <collision name='collision'>
                                                  <geometry>
                                                    <sphere>
                                                      <radius>{obstacle.Parameter["r"]}</radius>
                                                    </sphere>
                                                  </geometry>
                                                </collision>
		
                                                <visual name='visual'>
                                                  <geometry>
                                                    <sphere>
                                                      <radius>{obstacle.Parameter["r"]}</radius>
                                                    </sphere>
                                                  </geometry>
                                                </visual>
                                              </link>
                                            </model>");
                case "cylinder":
                    return XElement.Parse(@$"<model name='cylinder_{obstacle.Name}_R_{obstacle.Parameter["r"]}'>
                                              <pose>{obstacle.Parameter["x"]} {obstacle.Parameter["y"]} -0 0</pose>
                                              <link name='link'>
                                                <collision name='collision'>
                                                  <geometry>
                                                    <cylinder>
                                                      <radius>{obstacle.Parameter["r"]}</radius>
                                                      <length>{obstacle.Parameter["h"]}</length>
                                                    </cylinder>
                                                  </geometry>
                                                </collision>
		
                                                <visual name='visual'>
                                                  <geometry>
                                                    <cylinder>
                                                      <radius>{obstacle.Parameter["r"]}</radius>
                                                      <length>{obstacle.Parameter["h"]}</length>
                                                    </cylinder>
                                                  </geometry>
                                                </visual>
                                              </link>
                                           </model>");
            }

            ConsoleLogger.WriteLine("Unknown Obstacle Type '" + obstacle.Type + "'", Verbosity.Error);
            throw new ProcessingException();
        }
    }
}
