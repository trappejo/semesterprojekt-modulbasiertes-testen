﻿namespace compiler
{
    public interface IWriter
    {
        void Write(string path, TestParser.Node rootNode);
    }
}
