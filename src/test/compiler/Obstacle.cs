﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace compiler
{
    public class Obstacle
    {
        public Obstacle(string s)
        {
            if (!s.StartsWith("Obstacle", StringComparison.InvariantCultureIgnoreCase))
            {
                throw new ArgumentException();
            }

            s = s.Substring("Obstacle ".Length).Trim();
            var splitted = s.Split('=', 2);

            this.Name = splitted[0];
            s = splitted[1];
            splitted = s.Split('(', 2);
            this.Type = splitted[0].Trim().ToLower();
            this.Parameter = splitted[1].TrimEnd(')').Split(',').Select(x => x.Split('=', 2)).ToDictionary(x => x[0].Trim(), x => x[1].Trim());
        }

        public string Name { get; }
        public string Type { get; }
        public Dictionary<string, string> Parameter { get; }
    }

    public enum ObstacleType
    {

    }
}
