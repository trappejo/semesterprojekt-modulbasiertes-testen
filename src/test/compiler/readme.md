﻿# .NET Core

Die Neu-Implementierung von Microsofts .NET Plattform die so ziemlich überall läuft. Ja, auch auf Linux, MacOS, iOS, Android, ...und Windows   
Programmiert ist der Compiler in C#, welches zu .NET Plattform gehört. Das heißt, dass .NET u.A. den Compiler und ggf. die Runtime (vergleichbar mit Java, lässt sich aber auch als Standalone compilieren) zur Verfügung stellt.

Warum C#? Weil ich es am besten kann und es ganz nette Features wie LinQ zur Verfügung hat.

## Was brauche ich?

Im wesentlichen das _dotnet SDK_: https://dotnet.microsoft.com/download  
Für Linux gibt es auch PPAs usw.

## Wie compiliere ich?

Compilieren geht mit `dotnet build`, ausgeführt im Ordner wo die `.csproj` liegt.  
`dotnet run` compiliert und führt es direkt aus

Wer Visual Studio (das "große", nicht Code) kennt kann auch einfach die `.sln`-Datei damit öffnen. Für VS Code gibt es eine C#-Erweiterung.

## C# - was sollte ich wissen?

C# ist eine syntaktisch an C++ und Java angelehnte Programmiersprache, die sich erst in tiefergehenden Details deutlich unterscheidet. 
Angemerkt sei, dass Die Laufzeit von C# den Speicher frei räumt (vergleichbar mit Java), darum müssen wir uns also nicht kümmern. Eine Ausnahme bilden native Resourcen wie Streams, diese müssen manuell oder mit einem using-Block geschlossen werden.

Falls ihr etwas im Code verändert: Ich hoffe das meiste ist selbsterklärend. Wichtig sind vielleicht ein paar Sachen zu String:

Es gibt mehrere Arten von String

- `"Text"` - Der String beginnt und endet mit `"`  
  hier können Escapesequenzen wie `\n` verwendet werden um Sonderzeichen einzufügen. `\"` entspricht einem `"`
- `@"Text"` - so genannte Verbatim-Strings
  Escape-Sequenzen können hier nicht genutzt werden, dafür können Zeilenumbrüche direkt eingefügt werden.  
  Achtung: Auch Leerzeichen am Anfang einer Zeile werden 1 zu 1 mit übernommen  
  Um ein `"` einzugeben wird `""` benutzt  
  Diese String-Form ist besonders Praktisch um mehrzeiligen Code oder Pfadangaben mit \ wiederzugeben.
- `$"Test {var}"` bzw. `$@""` - so genannte Format-Strings  
  Es gelten die Grundregeln der jeweils anderen Formen. Zusätzlich können Variablen direkt in `{}` angegeben werden. Auf diesen Variablen wird dann `.ToString()` aufgerufen und das Ergebnis einfahc mit der linken- und rechten Seite verknüpft.

Weitere Informationen

- String-Operationen wie `Trim`, `Substring` usw. finden sich in der `System.String`-Klasse. 
- Formatierungen finden grundsätzlich in der Culture des Systems statt. Um Invariant zu bleiben (im wesentlichen en-US) muss `.ToString(CultureInfo.InvariantCulture)` genutzt werden
- Einzelne Zeichen (chars) werden in `'` angegeben.
