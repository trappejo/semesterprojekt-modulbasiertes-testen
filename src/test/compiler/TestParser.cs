﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace compiler
{
    public static class TestParser
    {
        public static IEnumerable<Node> ReadFile(string xmiFilePath)
        {
            ConsoleLogger.WriteLine($"Start parsing file '{xmiFilePath}'", Verbosity.Info);

            XNamespace xmiNS = "http://www.omg.org/spec/XMI/20131001";
            XNamespace umNS = "http://www.eclipse.org/uml2/5.0.0/UML";
            var doc = XDocument.Load(xmiFilePath);
            var packagedElement = doc.Root // selec Root-Node
                .Elements("packagedElement") // select all Sub-Elements `<packagedElement`
                .SingleOrDefault(x => x.Attribute(xmiNS + "type").Value == "uml:Activity"); // Select a single Element with `xmi:type="uml:Activity"`

            if (packagedElement == null)
            {
                ConsoleLogger.WriteLine($"Missing Activity chart in '{xmiFilePath}'", Verbosity.Error);
                yield break;
            }

            var comments = packagedElement.Elements("ownedComment")
                .Where(x => x.Attribute(xmiNS + "type").Value == "uml:Comment")
                .Select(x => new OwnedCommentData(x.Attribute(xmiNS + "id")?.Value, x.Attribute("annotatedElement")?.Value, x.Element("body")?.Value));

            var nodes = packagedElement.Elements("node")
                .Where(x => x.Attribute(xmiNS + "type").Value == "uml:OpaqueAction");

            var initialNodes = packagedElement.Elements("node")
                .Where(x => x.Attribute(xmiNS + "type").Value == "uml:InitialNode");

            var finalNodes = packagedElement.Elements("node")
                .Where(x => x.Attribute(xmiNS + "type").Value == "uml:FlowFinalNode");

            //edges aren't needed, XMI provides all required stuff on <node-Elements
            //var edges = packagedElement.Elements("edge")
            //    .Where(x => x.Attribute(xmiNS + "type").Value == "uml:ControlFlow");

            foreach (var initialNode in initialNodes)
            {
                var orderedList = new List<XElement>();
                // don't add initialNode, it does not have needed data

                var nextEdge = nodes.SingleOrDefault(x => x.Attribute("incoming").Value == initialNode.Attribute("outgoing").Value);
                while (nextEdge != null)
                {
                    orderedList.Add(nextEdge);
                    nextEdge = nodes.SingleOrDefault(x => x.Attribute("incoming").Value == nextEdge.Attribute("outgoing").Value);
                }

                var lastNode = orderedList.Last();
                nextEdge = finalNodes.SingleOrDefault(x => x.Attribute("incoming").Value == lastNode.Attribute("outgoing").Value);
                if (nextEdge == null)
                {
                    ConsoleLogger.WriteLine($"Missing Final-Node in '{xmiFilePath}'", Verbosity.Error);
                    continue;//skip propagation
                }

                var root = new Node();
                var last = root;
                foreach (var elem in orderedList)
                {
                    var newNode = new Node();
                    newNode.Text = elem.Attribute("name").Value;
                    var elemId = elem.Attribute(xmiNS + "id").Value;
                    newNode.Data = comments.SingleOrDefault(x => x.AnnotatedElement == elemId)?.BodyContent?.Split('\r', '\n');
                    last.Next = newNode;
                    last = newNode;
                }
                yield return root.Next;
            }
        }

        //inaccessible from outside of TestParser, used for deconstructing XMI file
        class OwnedCommentData
        {
            public string Id { get; set; }
            public string AnnotatedElement { get; set; }
            public string BodyContent { get; set; }

            public OwnedCommentData(string id, string annotatedElement, string bodyContent)
            {
                this.Id = id;
                this.AnnotatedElement = annotatedElement;
                this.BodyContent = bodyContent;
            }
        }


        public class Node: IEnumerable<Node>
        {
            public string Text { get; set; }

            public string[] Data { get; set; }

            public Node Next { get; set; }

            public IEnumerator<Node> GetEnumerator()
            {
                return new LinkedListEnumerator<Node>(this, x => x.Next);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.GetEnumerator();
            }
        }
    }
}
