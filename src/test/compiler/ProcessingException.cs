﻿using System;

namespace compiler
{
    public class ProcessingException : Exception
    {
        public ProcessingException() : base() { }

        public ProcessingException(string message) : base(message) { }

        public ProcessingException(string message, Exception innerException) : base(message, innerException) { }
    }
}
