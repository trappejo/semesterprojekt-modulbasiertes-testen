﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace compiler
{
    public class LinkedListEnumerator<T> : IEnumerator<T>
    {
        private Func<T, T> Next { get; }

        public LinkedListEnumerator(T element, Func<T, T> next)
        {
            this.First = element;
            this.Next = next;
        }

        public T First { get; }

        public T Current { get; set; }

        object IEnumerator.Current => throw new NotImplementedException();

        public virtual void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public bool MoveNext()
        {
            if (this.Current == null)
            {
                this.Current = this.First;
            }
            else
            {
                this.Current = this.Next(this.Current);
            }

            return this.Current != null;
        }

        public void Reset()
        {
            this.Current = this.First;
        }
    }
}
