﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;

namespace compiler
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleLogger.Verbosity = Verbosity.Debugging;
            CultureInfo.CurrentCulture = CultureInfo.InvariantCulture;

            if (args.Length != 2)
            {
                ConsoleLogger.WriteLine("Invalid Arguments", Verbosity.Error);
                return;
            }

            var sourceDirectory = args[0];
            var targetDirectory = args[1];

            if (Directory.Exists(targetDirectory))
            {
                Directory.Delete(targetDirectory, true);
            }

            var sourceFiles = Directory.GetFiles(sourceDirectory, "*.uml", SearchOption.AllDirectories);
            foreach (var sourceFile in sourceFiles)
            {
                var baseName = sourceFile.Substring(sourceDirectory.Length + 1);

                Console.WriteLine("Processing file: " + baseName);
                var rootNodes = TestParser.ReadFile(sourceFile).ToList();

                for (var i = 0; i < rootNodes.Count; i++)
                {
                    try
                    {
                        var rootNode = rootNodes[i];
                        var path = Path.Combine(targetDirectory, @$"{baseName}/{i}");
                        Directory.CreateDirectory(path);
                        TestWriter.WriteAll(path, rootNode);
                    }
                    catch (ProcessingException)
                    {
                        ConsoleLogger.WriteLine("An error occured. See above for details.", Verbosity.Error);
                    }
                    catch (Exception ex)
                    {
                        ConsoleLogger.WriteLine("An unexpected error occured: \n" + ex.Message + "\nStacktrac:" + ex.StackTrace, Verbosity.Error);
                    }
                }
            }
        }
    }
}