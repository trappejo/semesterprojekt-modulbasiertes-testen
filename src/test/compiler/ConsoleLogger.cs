﻿using System;

namespace compiler
{
    public static class ConsoleLogger
    {
        public static Verbosity Verbosity { get; set; }

        public static void WriteLine(string msg, Verbosity verbosity)
        {
            if (verbosity <= ConsoleLogger.Verbosity)
            {
                var color = ConsoleColor.White;
                switch (verbosity)
                {
                    case Verbosity.Debugging: color = ConsoleColor.Gray; break;
                    case Verbosity.Info: color = ConsoleColor.Cyan; break;
                    case Verbosity.Warning: color = ConsoleColor.Yellow; break;
                    case Verbosity.Error: color = ConsoleColor.Red; break;
                }

                DoInColor(color, () => Console.WriteLine(msg));
            }
        }

        private static void DoInColor(ConsoleColor color, Action action)
        {
            var currentColor = Console.ForegroundColor;
            Console.ForegroundColor = color;
            action();
            Console.ForegroundColor = currentColor;
        }
    }

    public enum Verbosity
    {
        Error = 1,
        Warning = 2,
        Info = 3,
        Debugging = 4
    }
}
