#!/bin/bash

 
# Vor erstem Start muss catkin_make in ~/catkin_ws ausgeführt werden

echo "testing ..."

## Vorbereitungen

# Log-Skripte ausführbar machen

chmod +x ~/catkin_ws/src/semesterprojekt-modulbasiertes-testen/src/sim/location_monitor/scripts/bot_listener.py
chmod +x ~/catkin_ws/src/semesterprojekt-modulbasiertes-testen/src/sim/location_monitor/scripts/location_monitor_node.py

# Die Art des Roboters festlegen (für dieses Beispiel mit turtlebot3 notwendig, für individuelle Verwendung bitte eventuell anpassen)

type=$(cat ~/catkin_ws/src/semesterprojekt-modulbasiertes-testen/build/sim/botname.txt)

# Für dieses Beispiel je nach 'type' die Länge vom Zentrum des Bots bis zur Vorderseite und die Höhe bestimmen

if [ "$type" = "burger" ]
	
	then

	botlaenge=0.06
	bothoehe=0.19

	else

	# neben 'burger' existieren im Beispiel nur 'waffle' und 'waffle_pi', für welche die Länge und die Höhe gleich ist

	botlaenge=0.07
	bothoehe=0.14

fi

# Navigation zum Verzeichnis mit den aktuellen Testfällen

cd ~/catkin_ws/src/semesterprojekt-modulbasiertes-testen/build/test/testfaelle

# Alle nötigen Pfadinformationen in Array 'testfaelle' eintragen

testfaelle=()

while IFS=  read -r -d $'\0'; do
    testfaelle+=("$REPLY")
done < <(find ./ -name "test.world" -print0)

# Anzahl der Testfälle ermitteln

anzahlTests=${#testfaelle[@]}


# Für jeden Testfall Simulation ausführen und Logs erstellen

for ((i=0; i<${anzahlTests}; i++)); do

	# Jeweiligen Eintrag in 'testfaelle' so kürzen, dass der Eintrag als Pfad verwendet werden kann

	path=$(echo ${testfaelle[$i]} | cut -d'/' -f-3)
	path=${path:2}

	# Aus gekürztem Eintrag für den Testfall entsprechend den Dateinamen bestimmen für 'worlds' und Logs

	filename=$(echo $path | tr -d /)	

	echo ""
	echo "Teste $path"
	echo ""

	# Für den jeweiligen Testfall die Spezifikation aus der config.ini erhalten 

	timeout=$(grep -i "timeout" ~/catkin_ws/src/semesterprojekt-modulbasiertes-testen/build/test/testfaelle/$path/config.ini | tr -d -c 0-9.)
	rightBorder=$(grep -i "rangeRightBorder=" ~/catkin_ws/src/semesterprojekt-modulbasiertes-testen/build/test/testfaelle/$path/config.ini | tr -d -c 0-9.)
	leftBorder=$(grep -i "rangeLeftBorder=" ~/catkin_ws/src/semesterprojekt-modulbasiertes-testen/build/test/testfaelle/$path/config.ini | tr -d -c 0-9.)

	# Safety Distance bestimmen anhand der gewonnenen Informationen

	dist=$(bc -l <<< "scale=10; ($rightBorder-$leftBorder)")

	# Jeweilige 'test.world' umbenannt ins sim-Verzeichnis verschieben

	cp ~/catkin_ws/src/semesterprojekt-modulbasiertes-testen/build/test/testfaelle/$path/test.world ~/catkin_ws/src/semesterprojekt-modulbasiertes-testen/build/sim/worlds/$filename.world

	# Launch-Datei dem Testfall entsprechend schreiben
	# Um die Simulation headless auszuführen bitte Zeile 98 mit '<arg name="gui" value="false"/>' ersetzen

	echo '<launch>
  <arg name="model" default="$(env TURTLEBOT3_MODEL)" doc="model type [burger, waffle, waffle_pi]"/>
  <arg name="x_pos" default="0"/>
  <arg name="y_pos" default="5"/>
  <arg name="z_pos" default="-0.001001"/>

  <include file="$(find gazebo_ros)/launch/empty_world.launch">
    <arg name="world_name" value="$(find sim)/worlds/'$filename'.world"/>
    <arg name="paused" value="false"/>
    <arg name="use_sim_time" value="true"/>
    <arg name="gui" value="true"/>
    <arg name="headless" value="false"/>
    <arg name="debug" value="false"/>
  </include>

  <param name="robot_description" command="$(find xacro)/xacro --inorder $(find sim)/bot.urdf.xacro" />

  <node pkg="gazebo_ros" type="spawn_model" name="spawn_urdf" args="-urdf -model turtlebot3_$(arg model) -x $(arg x_pos) -y $(arg y_pos) -z $(arg z_pos) -param robot_description" />
</launch>' > ~/catkin_ws/src/semesterprojekt-modulbasiertes-testen/build/sim/launch/$filename.launch


## Simulation

    # Sollte während der Iterationen die Simulation sich aufhängen, dann innerhalb des Hauptterminals Strg+C drücken um den aktuellen Testfall zu überspringen

	# Launch der Welt

	gnome-terminal -- bash -c 'cd ~/catkin_ws; source devel/setup.bash; export TURTLEBOT3_MODEL='$type'; roslaunch sim '$filename'.launch; sleep 5'

	# Bot Listener starten

	sleep 1
	gnome-terminal -- bash -c 'cd ~/catkin_ws; source devel/setup.bash; cd ~/catkin_ws/src/semesterprojekt-modulbasiertes-testen/build/sim/log; rosrun location_monitor bot_listener.py;'

	# Simulation starten

	gnome-terminal -- bash -c 'cd ~/catkin_ws; source devel/setup.bash; export TURTLEBOT3_MODEL=burger; rosrun sim turtlebot3_obstacle '$dist';'


	# Location Monitor starten

	cd ~/catkin_ws; source devel/setup.bash
	cd ~/catkin_ws/src/semesterprojekt-modulbasiertes-testen/build/sim/log

	echo "Location_Monitor:"
	echo ""

	# Breite von 'waffle' berücksichtigen

   	if [ "$type" = "burger" ] 
    
        	then

    		rosrun location_monitor location_monitor_node.py $botlaenge $bothoehe 0 0 $dist 500 $timeout
    	
    		else
    	
    		rosrun location_monitor location_monitor_node.py $botlaenge $bothoehe 0.12 55 $dist 500 $timeout
    	
    	fi

	# Nach Ende des Tests alle laufenden Skripte beenden

	ps ax | grep -i python | awk {'print $1'} | xargs kill -9 > /dev/null 2> /dev/null || :
	ps ax | grep -i gzclient | awk {'print $1'} | xargs kill -9 > /dev/null 2> /dev/null || :
	ps ax | grep -i gzserver | awk {'print $1'} | xargs kill -9 > /dev/null 2> /dev/null || :

	# Logs entsprechend umbenennen zur Unterscheidung

	mv ~/catkin_ws/src/semesterprojekt-modulbasiertes-testen/build/sim/log/bot_listener.log ~/catkin_ws/src/semesterprojekt-modulbasiertes-testen/build/sim/log/$filename'_bl'.log

	mv ~/catkin_ws/src/semesterprojekt-modulbasiertes-testen/build/sim/log/location_monitor.log ~/catkin_ws/src/semesterprojekt-modulbasiertes-testen/build/sim/log/$filename'_lm'.log

done

## Auswertung der Testfälle

cd ~/catkin_ws/src/semesterprojekt-modulbasiertes-testen/src/test/log_analyzer

chmod +x log_analyzer.py 

python3 log_analyzer.py -l ../../../build/sim/log/ -t ../../../build/test/testfaelle/


